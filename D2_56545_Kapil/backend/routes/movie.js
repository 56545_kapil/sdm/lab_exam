const express = require("express")
const utils = require("../utils")
const router = express.Router()
const { query } = require("express")

router.get("/:title", (request, response) => {
    const { title } = request.params

    const connection = utils.openConnection()

    const statement = `select * from movie WHERE title= '${title}'`
    connection.query(statement, (error, result) => {
        connection.end()
        if (result.length > 0) {
            console.log(result.length)
            console.log(result)
            response.send(utils.createResult(error, result))
        } else {
            response.send("user not found!!")
        }
    })
})

router.post("/add", (request, response) => {
    const { title, date, time, director } = request.body
    const connection = utils.openConnection()
    const statement = `insert into movie 
    (movie_title, movie_release_date, movie_time, director_name) values
    ('${title},${date},${time},${director})`

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.update("/update/:title", (request, response) => {
    const { date } = request.params
    const { time } = request.params
    const connection = utils.openConnection()
    const statement = `update movie 
                       set movie_release_date='${date}', movie_time='${time}'
                       where movie_title=('${title})`

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.delete("/remove/:title", (request, response) => {
    const { date } = request.params
    const { time } = request.params
    const connection = utils.openConnection()
    const statement = `delete from movie where movie_title=('${title})`

    connection.query(statement, (error, result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})
module.exports = router
