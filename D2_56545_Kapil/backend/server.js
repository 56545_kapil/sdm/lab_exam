const express = require("express")
const cors = require("cors")
const router=require("./routes/movie")
const app = express()

app.use(cors("*"))
app.use(express.json())
app.use("/movie",router)

app.listen(4000, "0.0.0.0", () => {
    console.log("server started on port 4000")
})