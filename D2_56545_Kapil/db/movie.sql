create table movie(movie_id integer primary key auto_increment, 
movie_title varchar(200), 
movie_release_date date,
movie_time varchar(50),
director_name varchar(100));